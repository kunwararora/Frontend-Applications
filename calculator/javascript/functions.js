export function computeFunc(currValue, preValue, operation) {
  let computation;
  const previousValue = parseFloat(preValue);
  const currentValue = parseFloat(currValue);
  if (isNaN(previousValue) && isNaN(currentValue)) return;

  switch (operation) {
    case "+":
      computation = previousValue + currentValue;
      break;
    case "-":
      computation = previousValue - currentValue;
      break;
    case "X":
      computation = previousValue * currentValue;
      break;
    case "/":
      computation = previousValue / currentValue;
      break;
    default:
      break;
  };

  return computation;
}
