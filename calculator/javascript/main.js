import Calculator from "./calculatorClass.js";

const previousValue = document.getElementById("previous-value");
const currentValue = document.getElementById("current-value");
const allClear = document.getElementById("all-clear-button");
const delButton = document.getElementById("del-button");
const equalToButton = document.getElementById("equal");
const operationButtons = document.getElementsByClassName("operational-button");
const numericButtons = document.getElementsByClassName("button");

let numButtons = [...numericButtons];
let opButtons = [...operationButtons];
const calculator = new Calculator(currentValue, previousValue);

numButtons.forEach(function(button) {
  button.addEventListener("click", (e) => {
    calculator.appendNum(e.target.innerText);
    calculator.updateDisplay();
  });
});

opButtons.forEach((button) => {
  button.addEventListener("click", function(e) {
    calculator.selectOperation(e.target.innerText);
    calculator.updateDisplay();
  });
});

allClear.addEventListener("click", function() {
  calculator.clearAll();
  calculator.updateDisplay();
});

delButton.addEventListener("click", function() {
  calculator.delete();
  calculator.updateDisplay();
});

equalToButton.addEventListener("click", function() {
  calculator.compute();
  calculator.updateDisplay();
});
