import { computeFunc } from "./functions.js";

export default class Calculator {
  constructor(currentValueTextElement, preValueTextElement) {
    this.currentValueTextElement = currentValueTextElement;
    this.preValueTextElement = preValueTextElement;
    this.clearAll();
  }

  clearAll() {
    this.currentValue = "";
    this.preValue = "";
    this.operation = null;
  }

  appendNum(number) {
    if (number === "." && this.currentValue.includes(".")) return;
    this.currentValue = this.currentValue.toString() + number.toString();
  }

  selectOperation(operation) {
    switch (true) {
      case this.currentValue === "" && this.preValue === "":
        this.isInterger(operation);
        break;
      case this.currentValue === "+" || this.currentValue === "-":
        this.currentValue = "";
        this.isInterger(operation);
        break;
      case this.preValue !== "":
        this.compute();
      default:
        this.operation = operation;
        if (this.currentValue != "") this.preValue = this.currentValue;
        this.currentValue = "";
        break;
    };

    return;
  }

  isInterger(intergerValue) {
    if (intergerValue === "+" || intergerValue === "-") {
      this.appendNum(intergerValue);
    }
    return;
  }

  compute() {
    if (this.currentValue === "" || this.preValue === "") return;
    let computation = computeFunc(
      this.currentValue,
      this.preValue,
      this.operation
    ); // calling function from another module

    this.currentValue = computation;
    this.operation = null;
    this.preValue = "";
  }

  updateDisplay() {
    this.currentValueTextElement.innerText = this.currentValue;
    this.preValueTextElement.innerText = this.preValue;
    if (this.operation != null) {
      this.preValueTextElement.innerText = `${this.preValueTextElement.innerText} ${this.operation}`;
    } else {
      this.preValueTextElement.innerText = "";
    }
  }

  delete() {
    this.currentValue = this.currentValue.toString().slice(0, -1);
  }
}
