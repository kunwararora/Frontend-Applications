let previousValue = document.getElementById('previous-value');
let currentValue = document.getElementById('current-value');
let operationButtons = document.getElementsByClassName('operational-button');
let numericButtons = document.getElementsByClassName('button');

let previousValueActive = false;
let operationalArray = [];   // holds value of currentValue and previousValue
let operation = '';
let newValue = false;
let intergerValue = false;
let enableButton = false;     // To disable/enable buttons like X and / 

for(numButton of numericButtons){
    numButton.addEventListener('click', (e)=>{
        if(newValue === true){
            reset();
        }
        currentValue.innerText += e.target.innerText;
        previousValueActive === false ? operationalArray.splice(0,1,parseFloat(currentValue.innerText)) : operationalArray.splice(1,2,parseFloat(currentValue.innerText));
        enableButton = true;
        intergerValue = true;
    })
};

for(opButton of operationButtons){
    opButton.addEventListener('click', (e)=>{
        let buttonText = e.target.innerText;
        if(newValue === true){
            reset();
        };
        switch(buttonText){
            case 'AC':
                reset();
                break;
            case '+':
                // for negative or positive value of currentValue
                if(!intergerValue){
                    currentValue.innerText = buttonText;
                    intergerValue = true;
                }
                else{
                    operate("add" ,buttonText);
                }
                break;
            case '-':
                // for negative or positive value of currentValue
                if(!intergerValue){
                    currentValue.innerText = buttonText;
                    intergerValue = true;
                }
                else{
                    operate("sub", buttonText);
                }
                break;
            case 'X':
                if(!enableButton){
                    reset();
                    break;
                }
                operate("multi", buttonText);
                break;
            case '/':
                if(!enableButton){
                    reset();
                    break;
                }
                operate("divide", buttonText);
                break;
            case '=':
                if(currentValue.innerText === ''){break;}
                previousValue.innerText += currentValue.innerText;
                let result = solve();
                currentValue.innerText = result.toString();
                newValue = true;
                break;
            case 'DEL':
                currentValue.innerText = currentValue.innerText.slice(0, -1);
                previousValueActive === false ? operationalArray.splice(0,1,parseFloat(currentValue.innerText)) : operationalArray.splice(1,2,parseFloat(currentValue.innerText));
                break;
            default:break;
        }
    })
};

// To solve all previous terms/values
function operate(operationKey, buttonText){
    if(previousValueActive === true){
        currentValue.innerText = ""
        let result = solve();
        operationalArray.splice(0, 1, result);
        previousValue.innerText = result.toString();
        previousValue.innerText += buttonText;
        operation = operationKey;
    }
    else{
        operationalArray.push(parseFloat(currentValue.innerText))
        currentValue.innerText += buttonText;
        previousValue.innerText = currentValue.innerText;
        previousValueActive = true;
        currentValue.innerText = '';
        operation = operationKey;
    };
};

// to get result of solving previous values
function solve(){
    let result = 0;
    switch(operation){
        case "add":
            result = operationalArray[0]+operationalArray[1];
            break;
        case "sub":
            result = operationalArray[0]-operationalArray[1];
            break;
        case 'multi':
            result = operationalArray[0]*operationalArray[1];
            break;
        case 'divide':
            result = operationalArray[0]/operationalArray[1];
            break;
        default:
            result = operationalArray[0];
            break;
    };
    return result;
};

function reset(){
    currentValue.innerText = "";
    previousValue.innerText = "";
    previousValueActive = false;
    intergerValue = false;
    enableButton = false;
    newValue = false;
    operationalArray = [];
    operation = "";
};
